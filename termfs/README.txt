
<h1 id="welcome-to-my-blog-">Welcome to my blog!</h1>
<p>Hi! its me Allure, your friendly neighborhood security enthusiast! </p>
<p>A short intro about me, a security and forensics masters graduate who&#39;s currently working as a Sysadmin - DevOps. I consider security should be a standardized systems and operation than a service.</p>
<p>Currently I&#39;m working on AD directory pentesting and also mobile pentesting. I really enjoy conveying my knowledge to people to understand the concepts of being a penetration tester or even practice security as a hobby. Hopefully someday I will be able to contribute to the industry with my knowledge!</p>
<p>I am also certified in OSCP, eCPPT, eWPTXv2 eJPT, and Sec+ </p>
<p class="has-line-data" data-line-start="54" data-line-end="55">Feel free to reach out to me on my telegram: <a href="https://t.me/Irene_says">https://t.me/Irene_says</a></p>
